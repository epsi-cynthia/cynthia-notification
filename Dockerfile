FROM node:12.10.0
WORKDIR /bin
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 10812
CMD npm start
