import * as express from 'express';
import {ApiOperationPost, ApiPath, SwaggerDefinitionConstant} from 'swagger-express-ts';
import {controller, httpPost, interfaces, requestBody} from 'inversify-express-utils';
import {body} from 'express-validator';
import {ControllerHelper, NotificationService} from '@services';
import {CREATED} from 'http-status-codes';
import {ENV, IErrorCode, KeycloakAuth} from '@shared';
import {inject} from 'inversify';
import {NotificationDto} from '@dtos';
import Controller = interfaces.Controller;

@ApiPath({
    path: '/notifications',
    name: 'Notification',
})
@controller(
    '/notifications',
    !(process.env.NODE_ENV || '').match(ENV.test) ? KeycloakAuth.getInstance().protect() : KeycloakAuth.getInstance().middleware(),
)
export class NotificationCtrl implements Controller {

    private static readonly ERROR_REQUIRED_MESSAGE = 'Le message est obligatoire.';

    constructor(
        @inject(NotificationService.name) private notificationService: NotificationService,
        @inject(ControllerHelper.name) private controllerHelper: ControllerHelper,
    ) {
    }

    @ApiOperationPost({
        summary: 'Push a new notification',
        description: 'Add a new notification in OneSignal and push it to destination user',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'Notification',
                required: true,
            },
        },
        responses: {
            201: {description: 'Created'},
            400: {description: 'Bad request'},
        },
    })
    @httpPost('',
        body('message').exists().withMessage(NotificationCtrl.ERROR_REQUIRED_MESSAGE),
    )
    public sendNotification(@requestBody() notification: NotificationDto, req: express.Request, res: express.Response) {
        if (this.controllerHelper.errorWithParams(req, res)) {
            return res;
        }

        return this.notificationService.sendNotification(notification)
            .then(() => res.sendStatus(CREATED))
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

}
