import {ApiModel, ApiModelProperty, SwaggerDefinitionConstant} from 'swagger-express-ts';

@ApiModel({
    name: 'Notification',
})
export class NotificationDto {

    @ApiModelProperty({
        description: 'Message of the notification',
        type: SwaggerDefinitionConstant.STRING,
        required: true,
    })
    public message: string;

    @ApiModelProperty({
        description: 'Segments of the notification (Subscribed Users,  Test Users...)',
        type: SwaggerDefinitionConstant.ARRAY,
        itemType: SwaggerDefinitionConstant.STRING,
    })
    public segments?: string[];

    constructor(message: string) {
        this.message = message;
    }

}
