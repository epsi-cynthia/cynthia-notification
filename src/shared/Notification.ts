import {
    ICreateNotificationResult,
    NotificationBySegmentBuilder,
    OneSignalAppClient,
} from 'onesignal-api-client-core';

export class Notification {

    private static client?: OneSignalAppClient;

    private message: string;
    private segments: string[];

    constructor(message: string, segments: string[]) {
        this.message = message;
        this.segments = segments;
    }

    private static getClientInstance(): OneSignalAppClient {
        if (!this.client) {
            this.client = new OneSignalAppClient(
                process.env.ONESIGNAL_APP_ID || '', process.env.ONESIGNAL_REST_API_KEY || '',
            );
        }

        return this.client;
    }

    public send(): Promise<ICreateNotificationResult> {
        const input = new NotificationBySegmentBuilder()
            .setIncludedSegments(this.segments)
            .notification()
            .setContents({ en: this.message })
            .build();

        return Notification.getClientInstance().createNotification(input);
    }

}
