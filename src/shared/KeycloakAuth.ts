import * as session from 'express-session';
import Keycloak from 'keycloak-connect';

export class KeycloakAuth {

    private static keycloak: Keycloak;

    private static init(): void {
        this.keycloak = new Keycloak({
            store: new session.MemoryStore(),
        }, {
            realm: process.env.KC_REALM,
            bearerOnly: true,
            authServerUrl: process.env.KC_AUTH_SERVER_URL,
            sslRequired: 'external',
            resource: process.env.KC_RESOURCE,
            confidentialPort: 0,
        });
    }

    public static getInstance(): Keycloak {
        if (!this.keycloak) {
            this.init();
        }

        return this.keycloak;
    }

}
