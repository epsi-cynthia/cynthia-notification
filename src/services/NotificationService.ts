import {ErrorCode, logger, Notification} from '@shared';
import {injectable} from 'inversify';
import {NotificationDto} from '@dtos';

@injectable()
export class NotificationService {

    public sendNotification(notificationDto: NotificationDto): Promise<any> {
        return new Promise((resolve, reject) => {
            const segments = notificationDto.segments || ['Subscribed Users'];
            const notification = new Notification(notificationDto.message, segments);

            notification.send()
                .then(() => resolve())
                .catch((err) => {
                    logger.error(err);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });
        });
    }

}
