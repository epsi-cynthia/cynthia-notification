import server from '@server';
import { logger } from '@shared';

(async () => {
    // Start the server
    const port = Number(process.env.PORT || 3000);
    const app = server.build();
    app.listen(port, () => {
        logger.info('Cynthia Notification started on port: ' + port);
    });
})();
