import server from '@server';
import supertest from 'supertest';

import { SuperTest, Test } from 'supertest';
import {NotificationDto} from '@dtos';

describe('Notification', () => {

    let agent: SuperTest<Test>;

    beforeAll((done) => {
        agent = supertest.agent(server.build());
        done();
    });

    it('Send notification', () => {
        const notification = new NotificationDto('Testing message');
        notification.segments = ['Test Users'];

        return agent.post('/notifications')
            .type('json')
            .send(notification)
            .expect(201);
    });

});
